sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (BaseController, JSONModel, formatter, Filter, FilterOperator) {
	"use strict";

	return BaseController.extend("com.arete.zwmuilabel.controller.Worklist", {

		formatter: formatter,

		
		onInit : function () {
			var oViewModel,
			demLogoImage = jQuery.sap.getModulePath("com.arete.zwmuilabel");
			

		

			oViewModel = new JSONModel({
				DemLogo: demLogoImage,
				Barcode: "",
				Stocks: [],
				fragment:"",
				enabledIc:false,
				enabledBar:false
				
				
				
			});
			this.setModel(oViewModel, "worklistView");

			
		},
		onUpdateFinished : function (oEvent) {
			var sTitle,
				oTable = oEvent.getSource(),
				iTotalItems = oEvent.getParameter("total");
			if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
				sTitle = this.getResourceBundle().getText("worklistTableTitleCount", [iTotalItems]);
			} else {
				sTitle = this.getResourceBundle().getText("worklistTableTitle");
			}
			this.getModel("worklistView").setProperty("/worklistTableTitle", sTitle);
		},
		onPressClear:function(){
			var oViewModel = this.getModel("worklistView");
			oViewModel.setProperty("/Barcode","");
			oViewModel.setProperty("/Stocks", "");
			that.onClearFunction();

		},
		onSubmitDepoAdres: function () {
			var that = this,
			 oDataModel = this.getModel(),
			 oViewModel = this.getModel("worklistView"),
			 inptBarcode=oViewModel.getProperty("/Barcode"),
			 processtype=2;

			oDataModel.callFunction("/GetStorageLocStock", {
				urlParameters: {
					"Lgpla": inptBarcode,
					"ProcessType": processtype
				},
				success: function(oData)
				{

					oViewModel.setProperty("/Stocks", oData.results);
				},
				error: function(oData)
				{
					alert("olmadı");
				}
				
			});
		},
		oBarkodType:function(oEvent){
			
			this.BarkodTipi=oEvent.getParameter("selectedItem").getKey();
			var that=this,
			 oValue = oEvent.getParameter("value"),
			 oViewModel=this.getModel("worklistView");
			if (oValue !== "") {
				that.getView().getModel("worklistView").setProperty("/enabledIc", true);
				that.getView().getModel("worklistView").setProperty("/enabledBar", true);
			}
			
			
			

		},
		onOpenDialog:function(oEvent)
		{
			var oViewModel = this.getModel("worklistView"),
			satir=oEvent.getSource().getBindingContext("worklistView").getObject();
			oViewModel.setProperty("/fragment", satir);
			 if (!this.newDialog) {
				                this.newDialog = new sap.ui.xmlfragment("com.arete.zwmuilabel.view.Dialog", this);
				                this.getView().addDependent(this.newDialog);

				            }
				
				            this.newDialog.open();
		},
		onCloseDialog:function(oEvent){
			this.newDialog.close();
			var oViewModel=this.getModel("worklistView");
			var that=this;
			that.onClearFunction();
			that.onEnableFunction();


		} ,
		onClearFunction:function(oEvent){
			debugger;	
			var oViewModel=this.getModel("worklistView");
			oViewModel.setProperty("/fragment/Barcodetype","");
			oViewModel.setProperty("/fragment/BarkodAdet","");					
			oViewModel.setProperty("/fragment/IcAdet","");
			
		},
		onEnableFunction:function(oEvent){
			var oViewModel=this.getModel("worklistView");
			oViewModel.setProperty("/enabledIc",false);
			oViewModel.setProperty("/enabledBar",false);

		},
		onPressBarkodla:function(oEvent)
		{
			var that = this,
			 oDataModel = this.getModel(),
			 oViewModel = this.getModel("worklistView"),
			 lqnum=oViewModel.getProperty("/fragment/Lqnum"),
			 inptMenge=oViewModel.getProperty("/fragment/Menge"),
			 inptparti=oViewModel.getProperty("/fragment/Charg"),
			 inptMaltan=oViewModel.getProperty("/fragment/Matnr"),
			 inptBarcodType=this.BarkodTipi,
			 inptBarkodMiktar=oViewModel.getProperty("/fragment/BarkodAdet"),
			inptIcmiktar=oViewModel.getProperty("/fragment/IcAdet");	
			
			
			if(inptBarkodMiktar!=="" && inptIcmiktar!=="")
			{
				if(parseInt(inptBarkodMiktar)*parseInt(inptIcmiktar)>inptMenge)
				{
					sap.m.MessageToast.show("Barkod Miktarı ile İç Miktar çarpımları Miktarı geçemez!");
	/* burada clearfunc için napmalıyım?? */	oViewModel.setProperty("/fragment/BarkodAdet","");					
					oViewModel.setProperty("/fragment/IcAdet","");					
					return;
				}
			}

			/*  oDataModel.callFunction("/CreateBarcode", {
				urlParameters: {
					"Lqnum": lqnum,
					"Charg": inptparti,
					"Matnr": inptMaltan,
					"BarcodeType": inptBarcodType,
					"InternalQuant": inptIcmiktar,
					"BarcodeQuant": inptBarkodMiktar


				},
				success: function(oData)
				{ */
					
					sap.m.MessageToast.show("İşlem Başarılı!");
					that.onSubmitDepoAdres();
					this.newDialog.close();
					var oViewModel=this.getModel("worklistView");
					that.onClearFunction();
					that.onEnableFunction();
					
			/* 	},
				error: function(oData)
				{
					
					alert("olmadı");
				}
				
			});   */
			
			}
			 
 
		
		

	});
});